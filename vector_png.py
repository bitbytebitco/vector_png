#!/usr/bin/python

import os
import subprocess
import argparse
import imghdr
import jpg_to_png
import change_opacity

def main(infile, outfile, color_count):
    os.chdir(os.getcwd())

    def subprocess_cmd(command):
        print(command)
        process = subprocess.Popen(command,stdout=subprocess.PIPE, shell=True)
        proc_stdout = process.communicate()[0].strip()
        print(proc_stdout)

    # check file type
    if imghdr.what(infile) != 'png':
        bn, ext = os.path.splitext(infile)
        jpg_to_png.convert(infile, bn) 

    # Command with shell expansion
    infile = os.path.join(os.getcwd(), infile)
    if os.path.exists(infile):
        mksvg = "autotrace -output-file temp.svg -output-format svg --color-count %s %s" % (color_count, infile)
        mkpng = "rsvg-convert temp.svg > %s" % outfile
        #commands = "%s ; %s" % (mksvg, mkpng)
        #print commands
        try:
            #subprocess.check_call(mksvg, stderr=subprocess.STDOUT, shell=True)
            subprocess_cmd(mksvg)
            subprocess_cmd(mkpng)
        except subprocess.CalledProcessError as e:
            print(e.output)
            print('mksvg error')
        finally:
            change_opacity.ReduceOpacity(infile)
        #    subprocess.check_call(mkpng)

        #os.remove("test.svg")
    else:
        print('file doesn\'t exist')

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Create pngs of vectorized images.')

    parser.add_argument('infile', help='input file')
    parser.add_argument('outfile', help='destination file')
    parser.add_argument('--color-count', type=int, help='color count')

    args = parser.parse_args()

    main(args.infile,args.outfile,args.color_count)
