# Description
vector_png.py is a utility that takes an image input (jpg,png) and produces multitone 
png of the vector tracing of them as an output. 

## Examples
* [My Dogs](https://bitbucket.org/bitbytebitco/vector_png/raw/78f91a9e496f55b467d19920f9d82d78555ac10c/images/mydogs.jpg) -> [My Dogs Traced](https://bitbucket.org/bitbytebitco/vector_png/raw/78f91a9e496f55b467d19920f9d82d78555ac10c/images/mydogs_traced.png)
* [Chonk](https://bitbucket.org/bitbytebitco/vector_png/raw/78f91a9e496f55b467d19920f9d82d78555ac10c/images/chonk.jpg) -> [Chonk Traced](https://bitbucket.org/bitbytebitco/vector_png/raw/78f91a9e496f55b467d19920f9d82d78555ac10c/images/chonk_traced.png)

## Dependencies
```python 
Pillow==7.2.0
pkg-resources==0.0.0
```

[rsvg-convert](http://manpages.ubuntu.com/manpages/trusty/man1/rsvg-convert.1.html)

[autotrace](https://github.com/autotrace/autotrace)

## Usage 

```bash
usage: vector_png.py [-h] [--color-count COLOR_COUNT] infile outfile

Create pngs of vectorized images.

positional arguments:
  infile                input file
  outfile               destination file

optional arguments:
  -h, --help            show this help message and exit
  --color-count COLOR_COUNT
                        color count
```
