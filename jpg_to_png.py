import os
from PIL import Image

def convert(infile, outname):
    fn, ext = os.path.splitext(infile)
    im = Image.open(infile)
    rgb_im = im.convert('RGB')
    rgb_im.save(outname + ".png", "PNG")
