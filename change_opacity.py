from PIL import Image, ImageEnhance

def ReduceOpacity(fn, opacity=1):
    im = Image.open(fn, "r")

    """
    Returns an image with reduced opacity.
    Taken from http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/362879
    """
    assert opacity >= 0 and opacity <= 1
    if im.mode != 'RGBA':
        im = im.convert('RGBA')
    else:
        im = im.copy()
    alpha = im.split()[3]
    alpha = ImageEnhance.Brightness(alpha).enhance(opacity)
    im.putalpha(alpha)
    im.save("/var/www/franchise_site/static/img/10003/test.png")
