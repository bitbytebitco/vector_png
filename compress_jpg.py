import sys
import argparse
from io import BytesIO
from PIL import Image

#size = 800,600

parser = argparse.ArgumentParser(description='script to compress jpgs')

parser.add_argument('infile', help='input file')
parser.add_argument('outfile', help='destination file')
parser.add_argument('-width', type=int, help='output width')
parser.add_argument('-comp', type=int, help='Compression level (0-100)')

args = parser.parse_args()

im = Image.open(args.infile)

print(im.width)
print(im.height)
print(im.width/im.height)
r = im.width/im.height
if 'width' in args:
    h = im.height*args.width/im.width
else: 
    h = im.height*1260/im.width
w = h*r
print('')
print(w)
print(h)
size = int(w),int(h)
print(size)

if im.mode != 'RGB':
    background = Image.new("RGB", (im.width,im.height), (255, 255, 255))
    background.paste(im, mask=im.split()[3])
    background.resize(size)
    background.save(args.outfile, "JPEG", quality=args.comp)
else:
    im.resize(size)
    im.save(args.outfile, "JPEG", quality=args.comp)

